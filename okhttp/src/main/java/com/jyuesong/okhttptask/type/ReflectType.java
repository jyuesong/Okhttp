package com.jyuesong.okhttptask.type;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Created by jiang on 2017/9/1.
 */

public class ReflectType {


    public static Type getClazzGenericType(Class clazz) {
        Type superClass = clazz.getGenericSuperclass();
        if (superClass instanceof Class) {
            throw new RuntimeException("Missing type parameter.");
        }
        ParameterizedType parameterizedType = (ParameterizedType) superClass;
        if (parameterizedType.getActualTypeArguments() == null || parameterizedType.getActualTypeArguments().length == 0) {
            throw new NullPointerException(clazz.getSimpleName() + " not has any generic type");
        }

        return parameterizedType.getActualTypeArguments()[0];


    }

}
