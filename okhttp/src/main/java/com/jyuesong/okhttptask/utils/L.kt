package com.jyuesong.okhttptask.utils

import android.util.Log
import com.jyuesong.okhttptask.BuildConfig

/**
 * Created by jiang on 2017/8/31.
 */


object L {


    fun e(msg: String, tag: String = "OKHttpTask") {
        if (BuildConfig.DEBUG) {
            Log.e(tag, msg)
        }

    }

}