package com.jyuesong.okhttptask.utils

import okhttp3.MediaType

/**
 * Created by jiang on 2017/9/12.
 */
object RequestType {

    val JSON = MediaType.parse("application/json; charset=utf-8")
}