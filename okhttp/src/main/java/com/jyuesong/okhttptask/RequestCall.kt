package com.jyuesong.okhttptask

import com.jyuesong.okhttptask.callback.CallBack
import okhttp3.*
import java.io.IOException

/**
 * Created by jiang on 2017/8/31.
 */

class RequestCall {


    private var mBuilder: Request.Builder
    private var id: Int? = null
    private var okhttpBuilder: OkHttpClient

    constructor(builder: Request.Builder, id: Int?, okhttpBuilder: OkHttpClient = OkHttpTask.instance().mOkHttpClient) {
        this.mBuilder = builder
        this.id = id
        this.okhttpBuilder = okhttpBuilder

    }

    fun execute(): Response {
        return okhttpBuilder.newCall(mBuilder.build()).execute()

    }


    fun <T> queue(callBack: CallBack<T>) {
        callBack.before()

        okhttpBuilder.newCall(mBuilder.build())
                .enqueue(object : Callback {
                    override fun onFailure(call: Call?, e: IOException?) {
                        callBack.onFailure(call, e, id)
                        OkHttpTask.instance().mHandler.post {
                            callBack.after()
                        }

                    }

                    override fun onResponse(call: Call?, response: Response?) {
                        callBack.onResponse(call, response, id)
                        OkHttpTask.instance().mHandler.post {
                            callBack.after()
                        }
                    }
                })

    }


}