package com.jyuesong.okhttptask.builder

import com.jyuesong.okhttptask.RequestCall
import okhttp3.Request

/**
 * Created by jiang on 2017/8/31.
 */
abstract class OkHttpBuilder<out T> {

    protected var url: String? = null
    protected var tag: Any? = null
    protected var headers: MutableMap<String, String>? = null
    protected var params: MutableMap<String, String>? = null
    protected var id: Int? = null


    protected var readTimeOut: Long = 5 * 60 //默认5分钟，防止出现大文件
    protected var writeTimeOut: Long = 5 * 60
    protected var connTimeOut: Long = 5 * 60


    fun readTimeOut(readTimeOut: Long) {
        this.readTimeOut = readTimeOut
    }

    fun writeTimeOut(writeTimeOut: Long) {
        this.writeTimeOut = writeTimeOut
    }

    fun connTimeOut(connTimeOut: Long) {
        this.connTimeOut = connTimeOut
    }

    fun id(id: Int): T {
        this.id = id

        return this as T
    }

    fun url(url: String): T {
        this.url = url
        return this as T
    }

    fun header(key: String?, value: String?): T {
        if (key != null && value != null) {
            if (headers != null) headers?.put(key, value) else headers = mutableMapOf(Pair(key, value))
        }

        return this as T
    }

    fun headers(headers: MutableMap<String, String>): T {
        this.headers = headers
        return this as T
    }

    fun params(params: MutableMap<String, String>): T {
        if (this.params == null) {
            this.params = params
        } else {
            this.params!!.putAll(params)
        }
        return this as T

    }

    fun param(key: String?, value: String?): T {
        if (key != null && value != null) {
            if (params != null) params?.put(key, value) else params = mutableMapOf(Pair(key, value))
        }

        return this as T
    }

    fun tag(tag: Any): T {
        this.tag = tag
        return this as T
    }

    abstract fun build(): RequestCall
    protected abstract fun makeRequest(): Request.Builder

}