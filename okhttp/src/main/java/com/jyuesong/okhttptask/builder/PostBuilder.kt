package com.jyuesong.okhttptask.builder

import android.util.Log
import com.jyuesong.okhttptask.RequestCall
import okhttp3.FormBody
import okhttp3.Headers
import okhttp3.Request
import okhttp3.RequestBody

/**
 * Created by jiang on 2017/8/31.
 */
class PostBuilder : OkHttpBuilder<PostBuilder>() {
    override fun makeRequest(): Request.Builder {
        val builder = Request.Builder()
        var requestBody = if (params != null && params!!.isNotEmpty()) {
//            RequestBody.create(RequestType.JSON, JSONObject(params).toString())
            val formBuilder = FormBody.Builder()
            for ((key, value) in params!!) {
                formBuilder.add(key, value)
            }
            formBuilder.build()
        } else {
            RequestBody.create(null, ByteArray(0))
        }

        builder.post(requestBody).url(url)
        headers?.let {
            builder.headers(Headers.of(headers))
        }
        tag?.let {
            builder.tag(tag)
        }
        Log.i(url, params.toString())
        return builder
    }


    override fun build(): RequestCall {

        return RequestCall(makeRequest(), id)

    }

}