package com.jyuesong.okhttptask.builder

import android.net.Uri
import com.jyuesong.okhttptask.RequestCall
import com.jyuesong.okhttptask.utils.L
import okhttp3.Headers
import okhttp3.Request

/**
 * Created by jiang on 2017/8/31.
 */
class GetBuilder : OkHttpBuilder<GetBuilder>() {
    override fun makeRequest(): Request.Builder {
        val builder = Request.Builder()
        builder.get().url(buildUrl(url, params))
        headers?.let {
            builder.headers(Headers.of(headers))
        }
        tag?.let {
            builder.tag(tag)
        }
        return builder
    }


    override fun build(): RequestCall {

        return RequestCall(makeRequest(), id)
    }

    private fun buildUrl(url: String?, params: MutableMap<String, String>?): String {

        url?.let {
            params?.let {
                val urlBuilder = Uri.parse(url).buildUpon()
                for ((key, value) in params) {
                    urlBuilder.appendQueryParameter(key, value)
                }
                return urlBuilder.build().toString()
            }

            return url
        }

        L.e("url is null so generated http://error")
        return "http://error"


    }

}