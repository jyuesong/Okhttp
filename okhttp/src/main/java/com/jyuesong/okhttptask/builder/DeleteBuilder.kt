package com.jyuesong.okhttptask.builder

import com.jyuesong.okhttptask.RequestCall
import okhttp3.FormBody
import okhttp3.Headers
import okhttp3.Request
import okhttp3.RequestBody

/**
 * Created by jiang on 2017/8/31.
 */
class DeleteBuilder : OkHttpBuilder<DeleteBuilder>() {
    override fun makeRequest(): Request.Builder {
        val builder = Request.Builder()
        var requestBody = if (params != null && params!!.isNotEmpty()) {
            val formBuilder = FormBody.Builder()
            for ((key, value) in params!!) {
                formBuilder.add(key, value)
            }
            formBuilder.build()
        } else {
            RequestBody.create(null, byteArrayOf(0))
        }

        builder.delete(requestBody).url(url)
        headers?.let {
            builder.headers(Headers.of(headers))
        }
        tag?.let {
            builder.tag(tag)
        }
        return builder
    }


    override fun build(): RequestCall {
        return RequestCall(makeRequest(), id)
    }


}