package com.jyuesong.okhttptask.builder

import com.jyuesong.okhttptask.OkHttpTask
import com.jyuesong.okhttptask.RequestCall
import okhttp3.*
import java.io.File
import java.util.concurrent.TimeUnit


/**
 * Created by jiang on 2017/8/31.
 */
class PostFileBuilder : OkHttpBuilder<PostFileBuilder>() {
    private var mProgressCallBack: ProgressCallBack? = null
    override fun makeRequest(): Request.Builder {
        if (mFile == null) {
            throw NullPointerException("file is nul")
        }
        var multiBodybuilder = MultipartBody.Builder().setType(MultipartBody.FORM)
        var requestBody: RequestBody? = RequestBody.create(MEDIA_TYPE_STREAM, mFile)
        if (mProgressCallBack != null) {
            requestBody = CountingRequestBody(RequestBody.create(MEDIA_TYPE_STREAM, mFile), CountingRequestBody.Listener { bytesWritten, contentLength -> mProgressCallBack!!.progress((bytesWritten * 1.0f / contentLength).toInt()) })
        }

        multiBodybuilder.addFormDataPart("file", mFile!!.name, requestBody)
        if (params != null && params!!.isNotEmpty()) {
            for ((key, value) in params!!) {
                multiBodybuilder.addFormDataPart(key, value)
            }
        }

        val builder = Request.Builder()
        builder.url(url)
        builder.post(multiBodybuilder.build())

        headers?.let {
            builder.headers(Headers.of(headers))
        }
        tag?.let {
            builder.tag(tag)
        }
        //这个给chain拿去用
        builder.header(KEY_NAME, mFile!!.name)
        builder.header(KEY_PATH, mFile!!.path)


        return builder
    }

    private var mFile: File? = null


    companion object {
        var MEDIA_TYPE_STREAM = MediaType.parse("application/octet-stream")
        val KEY_PATH = "file_path_chain"
        val KEY_NAME = "file_name_chain"
    }

    fun file(file: File): PostFileBuilder {
        mFile = file
        return this
    }

    fun file(file: String): PostFileBuilder {
        mFile = File(file)
        return this
    }


    fun progress(progress: ProgressCallBack): PostFileBuilder {
        mProgressCallBack = progress
        return this
    }


    override fun build(): RequestCall {


        return if (readTimeOut > 0 ||
                writeTimeOut > 0 ||
                connTimeOut > 0) {
            RequestCall(makeRequest(), id, OkHttpTask.instance().mOkHttpClient.newBuilder().readTimeout(readTimeOut, TimeUnit.SECONDS)
                    .writeTimeout(writeTimeOut, TimeUnit.SECONDS).connectTimeout(connTimeOut, TimeUnit.SECONDS).build())
        } else {
            RequestCall(makeRequest(), id, OkHttpTask.instance().mOkHttpClient.newBuilder().readTimeout(5, TimeUnit.MINUTES)
                    .writeTimeout(5, TimeUnit.MINUTES).connectTimeout(5, TimeUnit.MINUTES).build())
        }
    }


    interface ProgressCallBack {
        fun progress(progress: Int)
    }
}