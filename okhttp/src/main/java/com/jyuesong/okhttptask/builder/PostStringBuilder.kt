package com.jyuesong.okhttptask.builder

import com.jyuesong.okhttptask.RequestCall
import okhttp3.Headers
import okhttp3.MediaType
import okhttp3.Request
import okhttp3.RequestBody

/**
 * Created by jiang on 2017/8/31.
 */
class PostStringBuilder : OkHttpBuilder<PostStringBuilder>() {
    override fun makeRequest(): Request.Builder {
        val builder = Request.Builder()
        var requestBody: RequestBody = RequestBody.create(mediaType, ByteArray(0))

        if (!content.isNullOrEmpty())
            requestBody = RequestBody.create(mediaType, content)

        builder.post(requestBody).url(url)
        headers?.let {
            builder.headers(Headers.of(headers))
        }
        tag?.let {
            builder.tag(tag)
        }
        return builder
    }


    fun content(content: String): PostStringBuilder {
        this.content = content
        return this

    }

    private var content: String? = null
    var mediaType = MediaType.parse("text/plain;charset=utf-8")

    override fun build(): RequestCall {


        return RequestCall(makeRequest(), id)

    }

}