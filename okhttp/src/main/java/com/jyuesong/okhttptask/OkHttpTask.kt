package com.jyuesong.okhttptask

import android.os.Handler
import android.os.Looper
import com.jyuesong.okhttptask.builder.*
import okhttp3.OkHttpClient


/**
 * Created by jiang on 2017/8/31.
 */

class OkHttpTask {


    var mOkHttpClient: OkHttpClient


    val mHandler: Handler = Handler(Looper.getMainLooper())



    private constructor(okhttpClinet: OkHttpClient) {
        mOkHttpClient = okhttpClinet
    }


    companion object {
        @Volatile
        private var instance: OkHttpTask? = null

        fun initClient(okhttpClinet: OkHttpClient) {
            if (instance == null) {
                synchronized(OkHttpTask::class) {
                    if (instance == null) {
                        instance = OkHttpTask(okhttpClinet)
                    }
                }
            }
        }

        fun instance(): OkHttpTask {
            return instance!!
        }

        @JvmStatic
        fun get(): GetBuilder {
            return GetBuilder()
        }

        @JvmStatic
        fun post(): PostBuilder {
            return PostBuilder()
        }

        @JvmStatic
        fun put(): PutBuilder {
            return PutBuilder()
        }
        @JvmStatic
        fun postString(): PostStringBuilder {
            return PostStringBuilder()
        }
        @JvmStatic
        fun delete(): DeleteBuilder {
            return DeleteBuilder()
        }

        @JvmStatic
        fun upload(): PostFileBuilder {
            return PostFileBuilder()
        }

        @JvmStatic
        fun download(): DownloadBuilder {
            return DownloadBuilder()
        }

    }

    fun cancelTag(tag: Any) {
        mOkHttpClient.dispatcher().queuedCalls()
                .filter { tag == it.request().tag() }
                .forEach { it.cancel() }
        mOkHttpClient.dispatcher().runningCalls()
                .filter { tag == it.request().tag() }
                .forEach { it.cancel() }
    }


}