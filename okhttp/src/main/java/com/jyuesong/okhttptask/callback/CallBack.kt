package com.jyuesong.okhttptask.callback

import com.jyuesong.okhttptask.OkHttpTask
import okhttp3.Call
import okhttp3.Response
import java.io.IOException

/**
 * Created by jiang on 2017/8/31.
 */
abstract class CallBack<in T> {

    open fun before() {

    }

    open fun after() {

    }


    abstract fun error(call: Call?, e: String?, id: Int?)
    abstract fun success(t: T)
    open fun empty(id: Int?) {

    }

    /**
     * 只有当成功的时候才会解析返回值
     */
    abstract fun onResponse(call: Call?, response: Response?, id: Int?)

    open fun onFailure(call: Call?, e: IOException?, id: Int?) {
        OkHttpTask.instance().mHandler.post {
            error(call!!, "连接失败，请重试", id)
        }

    }


}