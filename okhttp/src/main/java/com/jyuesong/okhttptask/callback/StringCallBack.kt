package com.jyuesong.okhttptask.callback

import com.jyuesong.okhttptask.OkHttpTask
import okhttp3.Call
import okhttp3.Response
import org.json.JSONObject

/**
 * Created by jiang on 2017/8/31.
 */
abstract class StringCallBack : CallBack<String>() {

    override fun onResponse(call: Call?, response: Response?, id: Int?) {
        if (response != null) {
            if (response.isSuccessful) {
                val bodyStr = response.body()?.string()
                response.close()
                if (bodyStr.isNullOrEmpty() && call?.request()?.method().equals("GET")) {
                    emptyCallBack(id)
                } else {
                    successCallBack(bodyStr, id)
                }
            } else {
                val body = response.body()?.string()
                response.close()
                var msg = if (body.isNullOrEmpty()) {
                    response.message()
                } else {
                    val json = JSONObject(body)
                    json.getString("msg")
                }
                errorCallBack(call, msg, id)
            }
        } else {
            errorCallBack(call, "response is null", id)
        }


    }

    private fun errorCallBack(call: Call?, exception: String?, id: Int?) {
        OkHttpTask.instance().mHandler.post {
            error(call!!, exception, id)
        }
    }

    private fun emptyCallBack(id: Int?) {
        OkHttpTask.instance().mHandler.post {
            empty(id)
        }
    }

    private fun successCallBack(string: String?, id: Int?) {
        OkHttpTask.instance().mHandler.post {
            success(string!!)
        }

    }

}