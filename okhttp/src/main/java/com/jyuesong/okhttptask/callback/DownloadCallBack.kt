package com.jyuesong.okhttptask.callback

import com.jyuesong.okhttptask.OkHttpTask
import okhttp3.Call
import okhttp3.Response
import java.io.File
import java.io.FileOutputStream

/**
 * Created by jiang on 2017/8/31.
 */
abstract class DownloadCallBack : CallBack<String> {

    private val mPath: String
    private val mFileName: String


    constructor(path: String, fileName: String) : super() {
        this.mPath = path
        this.mFileName = fileName

    }

    override fun onResponse(call: okhttp3.Call?, response: Response?, id: Int?) {
        if (response?.isSuccessful!!) {
            val ins = response.body()!!.byteStream()
            var fos: FileOutputStream? = null
            try {
                var buff = ByteArray(2048)
                var length = response.body()!!.contentLength()

                if (length <= 0) {
                    errorCallBack(call, "contentlength is $length", id)
                    return
                }

                val path = File(mPath)
                if (!path.exists()) {
                    var successMakeDir = path.mkdirs()
                    if (!successMakeDir) {
                        errorCallBack(call, "create dir failed", id)
                        return
                    }

                }

                val file = File(mPath, mFileName)
                fos = FileOutputStream(file)

                var totalLength: Long = 0
                var lastProgress: Int = -1


                var len = ins.read(buff)
                while (len != -1) {
                    fos.write(buff, 0, len)
                    totalLength += len.toLong()
                    val progress = (totalLength * 100 / length).toInt()
                    if (progress != lastProgress) {
                        progressCallBack(totalLength, length, id)
                        lastProgress = progress
                    }
                    len = ins.read(buff)
                }
                fos.flush()
                successCallBack("下载成功", id)
            } catch (e: Exception) {
                e.printStackTrace()
                errorCallBack(call, e.message, id)
            } finally {
                try {
                    response.close()
                    ins?.close()
                    fos?.close()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

        } else {
            errorCallBack(call, response.message(), id)
        }

    }

    private fun errorCallBack(call: Call?, exception: String?, id: Int?) {
        OkHttpTask.instance().mHandler.post {
            error(call!!, exception, id)
        }
    }

    private fun progressCallBack(progress: Long, total: Long, id: Int?) {
        OkHttpTask.instance().mHandler.post {
            progress(progress, total, id)
        }
    }

    abstract fun progress(progress: Long, total: Long, id: Int?)

    private fun successCallBack(string: String?, id: Int?) {
        OkHttpTask.instance().mHandler.post {
            success(string!!)
        }

    }
}